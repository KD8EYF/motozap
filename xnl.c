/*
 
 * File:   xnl.c
 * Description: common xnl functions

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. */

#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>
#include "xnl.h"
#include "xnlauth.h"

t_xnlHeader authKeyRequest(int srcAddr, int dstAddr) {
        t_xnlHeader returnData;
        returnData.length = htons(sizeof(returnData) - 2);
        returnData.xnlOpcode=htons(XNLDEVICEAUTHKEYREQUEST);
        returnData.proto = XNL;
        returnData.flags = 0xf0;
        returnData.dstAddr = htons(dstAddr);
        returnData.srcAddr = htons(srcAddr);
        returnData.devID = 0;
        returnData.msgID = 0;
        returnData.payloadLen = htons(sizeof(returnData) - 14);
        return returnData;
}
uint64_t ntohll(uint64_t n) {
	uint64_t retval;
	retval = ((uint64_t) ntohl(n & 0xFFFFFFFFLLU)) << 32;
	retval |= ntohl((n & 0xFFFFFFFF00000000LLU) >> 32);
	return(retval);
}
uint64_t htonll(uint64_t n) {
        uint64_t retval;
        retval = ((uint64_t) htonl(n & 0xFFFFFFFFLLU)) << 32;
	retval |= htonl((n & 0xFFFFFFFF00000000LLU) >> 32);
	return(retval);
}
uint64_t encryptBytes(uint64_t bytes){
        int i, sum = 0;
        uint32_t v0 = bytes >> 32;
        uint32_t v1 = bytes & 0xFFFFFFFF;
        uint32_t k0 = key0;
        uint32_t k1 = key1;
        uint32_t k2 = key2;
        uint32_t k3 = key3;
        for (i=0; i < 32; i++) {
                sum += delta;
                v0 += ((v1<<4) + k0) ^ (v1 + sum) ^ ((v1>>5) + k1);
                v1 += ((v0<<4) + k2) ^ (v0 + sum) ^ ((v0>>5) + k3);
        }
        return (uint64_t) v0 << 32 | v1;
};
t_devConnRequest devConnRequest(int srcAddr, int dstAddr, uint64_t authBytes, int deviceType, int prefAddr, int authIndex) {
	t_devConnRequest returnData;
	uint64_t encryptedBytes = encryptBytes(ntohll(authBytes));
	returnData.xnlHeader.length = htons(sizeof(returnData) - 2);
	returnData.xnlHeader.xnlOpcode = htons(XNLDEVICECONNREQUEST);
	returnData.xnlHeader.proto = XNL;
        returnData.xnlHeader.flags =  8;
        returnData.xnlHeader.dstAddr = htons(dstAddr);
	returnData.xnlHeader.srcAddr = htons(srcAddr);
	returnData.xnlHeader.devID = 0;
	returnData.xnlHeader.msgID = 0;
	returnData.xnlHeader.payloadLen = htons(sizeof(returnData) - 14);
	returnData.xnlAddr = htons(prefAddr);
	returnData.deviceType = deviceType;
	returnData.authIndex = authIndex;
	returnData.bytes = htonll(encryptedBytes);
	return returnData;
}

t_fzreq1 fzreq1(int srcAddr, int dstAddr, int flag, int dev) {
	t_fzreq1 returnData;
        returnData.xnlHeader.length = htons(sizeof(returnData) - 2);
        returnData.xnlHeader.xnlOpcode = htons(XNLDATAMSG);
        returnData.xnlHeader.proto = XMCP;
        returnData.xnlHeader.flags =  flag;
        returnData.xnlHeader.dstAddr = htons(dstAddr);
        returnData.xnlHeader.srcAddr = htons(srcAddr);
        returnData.xnlHeader.devID = dev;
        returnData.xnlHeader.msgID = 0;
        returnData.xnlHeader.payloadLen = htons(sizeof(returnData) - 14);
        returnData.xmcpOpcode = 2;
        returnData.xmcpMsg = 0;
        return returnData;
}
