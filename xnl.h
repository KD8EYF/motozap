/*
 
 * File:   xnl.h
 * Description: xnl function header file

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. */

#ifndef XNL_H
#define	XNL_H

#ifdef	__cplusplus
extern "C" {
#endif

#define XNLMASTERSTATUSBRDCST   0x0002
#define XNLDEVICEMASTERQUERY    0x0003
#define XNLDEVICEAUTHKEYREQUEST 0x0004
#define XNLDEVICEAUTHKEYREPLY   0x0005
#define XNLDEVICECONNREQUEST    0x0006
#define XNLDEVICECONNREPLY      0x0007
#define XNLDEVICESYSMAPREQUEST  0x0008
#define XNLDEVICESYSMAPBRDCST   0x0009
#define XNLDATAMSG              0x000B
#define XNLDATAMSGACK           0x000C
#define XNL			0x00
#define XMCP			0x01

    typedef struct str_xnlHeader {
        unsigned short int length;
        unsigned short int xnlOpcode;
        unsigned char proto;
        unsigned char flags;
        unsigned short int dstAddr;
        unsigned short int srcAddr;
        unsigned char devID;
        unsigned char msgID;
        unsigned short int payloadLen;
    } __attribute__((packed)) t_xnlHeader;

    typedef struct str_devConnRequest {
        struct str_xnlHeader xnlHeader;
        unsigned short int xnlAddr;
        unsigned char deviceType;
        unsigned char authIndex;
        uint64_t bytes;
    } __attribute__((packed)) t_devConnRequest;

    typedef struct xnlBits {
        unsigned short int ackID : 3;
        unsigned int ackEnable : 1;
        unsigned int ackReserved : 4;
    } xnlBits;

    typedef union {
        xnlBits bits;
        unsigned short int integer;
    } xnlFlag;

    struct __attribute__((__packed__)) str_authKeyReply {
        struct str_xnlHeader xnlHeader;
        unsigned short int xnlAddr;
        uint64_t randBytes;
    };

    struct __attribute__((__packed__)) str_xmcpDataMsg {
        struct str_xnlHeader xnlHeader;
        unsigned short int opcode;
    };

    struct __attribute__((__packed__)) str_rssi {
        struct str_xmcpDataMsg xcmpMsg;
        unsigned short int replyType;
        unsigned char rssi1;
        unsigned char rssi2;
    };

    struct __attribute__((__packed__)) str_devConnReply {
        struct str_xnlHeader xnlHeader;
        unsigned char resultCode;
        unsigned char TransIdBase;
        unsigned short int xnlAddr;
        unsigned char deviceType;
        unsigned char logicalAddr;
        uint32_t encBytes;
    };

    typedef struct xmcpBits {
        unsigned short int messageType : 4;
        unsigned short int opcode : 12;
    } xmcpBits;

    typedef union {
        xmcpBits bits;
        unsigned char integer;
    } OpcodeBits;

    struct __attribute__((__packed__)) str_radioStatusRequest {
        struct str_xnlHeader xnlHeader;
        unsigned short int xmcpOpcode;
        unsigned char condition;
    };

    struct __attribute__((__packed__)) str_devIntSts {
        struct str_xnlHeader xnlHeader;
        unsigned char xmcpOpcode;
        unsigned char xmcpMsg;
        uint32_t version;
        unsigned char devType;
        unsigned char b1;
        unsigned char b2;
        unsigned char b3;
        unsigned char b4;
    };

    typedef struct str_devQuery {
        struct str_xnlHeader xnlHeader;
        unsigned char xmcpOpcode;
        unsigned char xmcpMsg;
        unsigned char condition;
    } __attribute__((packed)) t_devQuery;

    typedef struct str_fzreq1 {
        struct str_xnlHeader xnlHeader;
        unsigned char xmcpOpcode;
        unsigned char xmcpMsg;
        
    }__attribute__((packed)) t_fzreq1;
    
    t_devQuery deviceQuery(int srcAddr, int dstAddr, int condition, int flag, int dev, int sync, int msg);
    t_xnlHeader authKeyRequest(int srcAddr, int dstAddr);
    uint64_t encryptBytes(uint64_t bytes);
    t_devConnRequest devConnRequest(int srcAddr, int dstAddr, uint64_t authBytes, int deviceType, int prefAddr, int authIndex);
    void printHexline(unsigned char *payload, int dir, int size, int type);
    t_fzreq1 fzreq1(int srcAddr, int dstAddr, int flag, int dev);

#ifdef	__cplusplus
}
#endif

#endif	/* XNL_H */

