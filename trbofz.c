/*
 
 * File:   trbofz.c
 * Description: Reboot mototrbo radio into Flashzap mode

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "xnl.h"

#define DEVICETYPE 0x0a
#define BUFFSIZE 1024
#define PREFERXNLADDR 0xBEEF
#define AUTHINDEX 0

int main(int argc, char** argv) {
    setvbuf(stdout, NULL, _IONBF, 0);
    unsigned short int sock;
    unsigned short int connected = 0;
    unsigned short int rxOpcode = 0;
    unsigned short int srcAddr = 0;
    unsigned short int dstAddr = 0;
    unsigned short int devID = 0;
    unsigned short int xnlAddr = 0;
    unsigned char rxData[1024];

    struct sockaddr_in radio_addr;

    struct str_xnlHeader *rxHeader;
    if (argc != 2) {
        fprintf(stderr, "trbofz (boot radio into flashzap mode)\nusage: %s <ip of radio>\n", argv[0]);
        exit(0);
    }

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Socket");
        exit(1);
    }
   
    radio_addr.sin_family = AF_INET;
    radio_addr.sin_port = htons(8002);
    radio_addr.sin_addr.s_addr = inet_addr(argv[1]);
    bzero(&(radio_addr.sin_zero), 8);
    if (connect(sock, (struct sockaddr *) &radio_addr,
            sizeof (struct sockaddr)) == -1) {
        perror("Connect");
        exit(1);
    }
    while (connected != 1) {
        recv(sock, rxData, 1024, 0);
        rxHeader = (struct str_xnlHeader*) rxData;
        rxOpcode = ntohs(rxHeader->xnlOpcode);
        srcAddr = ntohs(rxHeader->dstAddr);
        dstAddr = ntohs(rxHeader->srcAddr);
        if (rxOpcode == XNLMASTERSTATUSBRDCST) {

            t_xnlHeader txMsg = authKeyRequest(srcAddr, dstAddr);
            unsigned char txData[sizeof (txMsg)];
            memcpy(txData, &txMsg, sizeof (txMsg));

            send(sock, txData, sizeof (txData), 0);
        }
        if (rxOpcode == XNLDEVICEAUTHKEYREPLY) {

            struct str_authKeyReply *authKeyReply = (struct str_authKeyReply*) rxData;
            t_devConnRequest txMsg = devConnRequest(srcAddr, dstAddr, authKeyReply->randBytes, DEVICETYPE, PREFERXNLADDR, AUTHINDEX);
            unsigned char txData[sizeof (txMsg)];
            memcpy(txData, &txMsg, sizeof (txMsg));

            send(sock, txData, sizeof (txData), 0);
        }
        if (rxOpcode == XNLDEVICECONNREPLY) {

            struct str_devConnReply *devConnReply = (struct str_devConnReply*) rxData;
            if (devConnReply->resultCode == 1) {
                printf("CONNECTED TO RADIO\n");

                devID = devConnReply->TransIdBase;
                xnlAddr = ntohs(devConnReply->xnlAddr);
            } else {
                printf("CONNECTION REFUSED BY RADIO\n");
                exit(1);
            };
        };
        if (rxOpcode == XNLDEVICESYSMAPBRDCST) {
        };
        if (rxOpcode == XNLDATAMSG) {
            struct str_xmcpDataMsg *xmcpDataMsg = (struct str_xmcpDataMsg*) rxData;
            if (ntohs(xmcpDataMsg->opcode) == 0xb41c) {
                connected = 1;
            };
        };
    };


    if (connected == 1) {
        printf("SENDING REBOOT REQUEST\n");
        t_fzreq1 txMsg = fzreq1(xnlAddr, dstAddr, 2, devID);
        unsigned char txData[sizeof (txMsg)];
        memcpy(txData, &txMsg, sizeof (txMsg));
        send(sock, txData, sizeof (txData), 0);
        close(sock);
        return 0;
    } else {
        return 1;
    }


};



